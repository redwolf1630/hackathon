class Donor < ActiveRecord::Base
  has_many :donations

  def total
   donations.sum(:amount)
  end
end
