class Donation < ActiveRecord::Base
belongs_to :donor


  def self.total_donations
    Donation.sum(:amount)
  end
end
